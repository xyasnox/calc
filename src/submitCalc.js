import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
  color: palevioletred;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  background-color: white;
`;

const SubmitCalc = ({
                      onClick,
                      result
                    }) =>
  <div>
    <Button onClick={() => onClick()}>Calculate!!!!!!!!!!!!!!!!!</Button>
    <p>{result}</p>
  </div>;

export default SubmitCalc;
