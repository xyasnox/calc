import React from 'react';
import styled from 'styled-components';

const InputForm = styled.input`
  color: palevioletred;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  background-color: white;
`;

const Input = ({
                 onChange,
                 inputName,
                 value
               }) =>
  <div>
    <InputForm className='number'
               type='text'
               onChange={e => onChange(e.target.value, inputName)} />
    <p>{value}</p>
  </div>;

export default Input;
